var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var fs = require('fs');
var creds = '';
var redis = require('redis');
var client = '';
var orderbook = '';

// Read credentials from JSON
fs.readFile('creds.json', 'utf-8', function(err, data) {
    if (err) throw err;
    creds = JSON.parse(data);
    client = redis.createClient({ 'host': creds.host, 'port': creds.port, 'password': creds.password });

    // Redis Client Ready
    client.once('ready', function() {

        // // Flush Redis DB
        client.flushdb();
        fs.readFile('orderbook.json', 'utf-8', function(error, result) {
            if (error) throw error;
            orderbook = JSON.parse(result);

            //using multi to allow several calls in batch
            var multi = client.multi();

            //to autopopulate the orderbook from orderbook.json
            for (const buy_order of orderbook.buy_orderbook) {
                multi.zadd("buy_order", Math.round(buy_order.price * 10000), JSON.stringify(buy_order));
            }
            for (const sell_order of orderbook.sell_orderbook) {
                multi.zadd("sell_order", Math.round(sell_order.price * 10000), JSON.stringify(sell_order));
            }
            multi.exec(function(err, replies) {
                console.log(replies)
            });
        })

        // //setting total number of available currency to trade
        client.set("available_buy_coin", "20", function(err) {
            if (err) {
                // Something went wrong
                console.error("error");
            } else {
                client.get("available_buy_coin", function(err, value) {
                    if (err) {
                        console.error("error");
                    } else {
                        console.log("Worked: " + value);
                    }
                });
            }
        });

        client.set("available_sell_coin", "20", function(err) {
            if (err) {
                // Something went wrong
                console.error("error");
            } else {
                client.get("available_sell_coin", function(err, value) {
                    if (err) {
                        console.error("error");
                    } else {
                        console.log("Worked: " + value);
                    }
                });
            }
        });

    });
});

var port = process.env.PORT || 8080;

// Express Middleware for serving static
// files and parsing the request body
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));

// Start the Server
http.listen(port, function() {
    console.log('Server Started. Listening on *:' + port);
});


// Render Main HTML file
app.get('/', function(req, res) {
    res.sendFile('views/index.html', {
        root: __dirname
    });
});


// Socket Connection
io.on('connection', function(socket) {
    socket.on('transaction', function(data) {
        if (data.order_type == 'BUY') {
            client.get("available_sell_coin", function(err, value) {
                if (err) {
                    console.log(err);
                } else {
                    //checking if available sell coin is greater than buy amount
                    if (parseFloat(value) >= data.amount) {
                    	var transaction_id = Math.floor(1000 + Math.random() * 9000);
                        var amount = data.amount;
                        var bought_amount = 0.0;
                        //logic to do transactions
                        var multi = client.multi();
                        client.zrange("sell_order", 0, -1, function(err, rep) {
                            if (rep.length == 0) {
                                io.emit('transaction_successful', { "status": "failed", "msg": "Not enough coins to trade" });
                            } else {
                                var i = 0;
                                while (true) {
                                    var item = JSON.parse(rep[i]);
                                    var result = {};
                                    
                                    //console.log(item);
                                    if (parseFloat(amount) <= parseFloat(item.order_amount)) {
                                        //add transaction in multi
                                        multi.zrem("sell_order", JSON.stringify(item));
                                        item.order_amount = item.order_amount - amount;
                                        
                                        if (parseFloat(item.order_amount) != 0) {
                                            multi.zadd("sell_order", Math.round(item.price * 10000), JSON.stringify(item));
                                        }
                                        bought_amount = parseFloat(bought_amount) + parseFloat(amount)*parseFloat(item.price);
                                        result.transaction_id = transaction_id;
                                        result.amount = amount;
                                        result.transaction_type = "COMPLETE_BUY";
                                        result.status = "Successful";
                                        result.timestamp = (new Date()).getTime();
                                        result.bought_amount = bought_amount;
                                        multi.zadd("transaction:002", (new Date()).getTime() + i, JSON.stringify(result));
                                        break;
                                    } else {
                                        multi.zrem("sell_order", JSON.stringify(item));
                                        bought_amount = parseFloat(bought_amount) + parseFloat(item.order_amount)*parseFloat(item.price);
                                        amount = amount - item.order_amount;
                                        result.transaction_id = transaction_id;
                                        result.amount = item.order_amount;
                                        result.transaction_type = "PARTIAL_BUY";
                                        result.status = "Successful";
                                        result.timestamp = (new Date()).getTime();
                                        multi.zadd("transaction:002", (new Date()).getTime() + i, JSON.stringify(result));
                                        i++;
                                    }
                                }
                                //End of while
                                multi.set("available_sell_coin", parseFloat(value) - parseFloat(data.amount));
                                multi.zrange("transaction:002", 0, -1, );
                                multi.exec(function(err, replies) {
                                    io.emit('transaction_successful', replies[replies.length - 1]);
                                });
                            }
                        });

                    } else {
                        //send response back to client with payload indicating not enough amount in active orderbook
                        io.emit('transaction_successful', { "status": "failed", "msg": "Not enough coins to trade" });
                    }
                }
            })
        } else {
            client.get("available_buy_coin", function(err, value) {
                if (err) {
                    console.log(err);
                } else {
                    //checking if available sell coin is greater than buy amount
                    if (parseFloat(value) >= data.amount) {
                    	var transaction_id = Math.floor(1000 + Math.random() * 9000);
                    	var sold_amount = 0.0;
                        var amount = data.amount;
                        //logic to do transactions
                        var multi = client.multi();
                        client.zrevrange("buy_order", 0, -1, function(err, rep) {
                            if (rep.length == 0) {
                                io.emit('transaction_successful', { "status": "failed", "msg": "Not enough coins to trade" });
                            } else {
                                var i = 0;
                                while (true) {
                                    var item = JSON.parse(rep[i]);
                                    var result = {};
                                    //console.log(item);
                                    if (parseFloat(amount) <= parseFloat(item.order_amount)) {
                                        //add transaction in multi
                                        multi.zrem("buy_order", JSON.stringify(item));
                                        item.order_amount = item.order_amount - amount;
                                        if (parseFloat(item.order_amount) != 0) {
                                            multi.zadd("buy_order", Math.round(item.price * 10000), JSON.stringify(item));
                                        }
                                        sold_amount = parseFloat(sold_amount) + parseFloat(amount)*parseFloat(item.price);
                                        result.transaction_id = transaction_id;
                                        result.amount = amount;
                                        result.transaction_type = "COMPLETE_SELL";
                                        result.sold_amount = sold_amount;
                                        result.status = "Successful";
                                        result.timestamp = (new Date()).getTime();
                                        multi.zadd("transaction:002", (new Date()).getTime() + i, JSON.stringify(result));
                                        break;
                                    } else {
                                        multi.zrem("buy_order", JSON.stringify(item));
                                        amount = amount - item.order_amount;
                                        sold_amount = parseFloat(sold_amount) + parseFloat(item.order_amount)*parseFloat(item.price);
                                        result.transaction_id = transaction_id;
                                        result.amount = item.order_amount;
                                        result.transaction_type = "PARTIAL_SELL";
                                        result.status = "Successful";
                                        result.timestamp = (new Date()).getTime();
                                        multi.zadd("transaction:002", (new Date()).getTime() + i, JSON.stringify(result));
                                        i++;
                                    }
                                }
                                //End of while
                                multi.set("available_buy_coin", parseFloat(value) - parseFloat(data.amount));
                                multi.zrange("transaction:002", 0, -1);
                                multi.exec(function(err, replies) {
                                    io.emit('transaction_successful', replies[replies.length - 1]);
                                });
                            }
                        });

                    } else {
                        //send response back to client with payload indicating not enough amount in active orderbook
                        io.emit('transaction_successful', { "status": "failed", "msg": "Not enough coins to trade" });
                    }
                }
            })
        }
    });
});





