$(function() {
    // test
    var socket = io();
    var order_type

    $('#submit-order').click(function() {
        order_type = $("#order-type option:selected").text();
        var amount = $("#amount").val();
        socket.emit('transaction', {
            'order_type': order_type,
            'amount': amount
        });
    });

    socket.on('transaction_successful', function(data) {
    	console.log(JSON.stringify(data));
    	if(data.status != undefined){
    		alert(data.msg);
    	} else{

        var col = ['transaction_id', 'amount', 'transaction_type', 'status', 'timestamp', 'traded_amount'];

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < data.length; i++) {

            tr = table.insertRow(-1);

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                var temp = JSON.parse(data[i]);
                if(col[j] == 'timestamp'){
                	var date = new Date(temp[col[j]]);
                	tabCell.innerHTML = date;
                } else if(col[j] == 'traded_amount'){
                	if(temp['transaction_type'] == 'COMPLETE_SELL'){
                		tabCell.innerHTML = temp['sold_amount'];
                	} else if(temp['transaction_type'] == 'COMPLETE_BUY'){
                		tabCell.innerHTML = temp['bought_amount'];
                	} else{
                		tabCell.innerHTML = 'N/A';
                	}
                } else{
                	tabCell.innerHTML = temp[col[j]];
                }
            }
        }

        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("result");
        divContainer.innerHTML = "";

        divContainer.appendChild(table);
   }
    });
});